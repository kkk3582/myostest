%include "./boot/include/boot.inc"

SECTION MBR vstart=0x7c00
	mov ax,cs
	mov ds,ax
	mov es,ax
	mov ss,ax
	mov fs,ax
	mov sp,0x7c00
	mov ax,0xb800
	mov gs, ax



	mov ax, 0x0600
	mov bx, 0x0700
	mov cx, 0x0000	;左上角 y，x坐标（00,00）
	mov dx, 0x184f	;右下角 y,x 坐标（0x18=24,0x4f=79)
	int 0x10


	mov byte [gs:0x00],'M'
	mov byte [gs:0x01],0xa4

	mov byte [gs:0x02],'B'
	mov byte [gs:0x03],0xa4

	mov byte [gs:0x04],'R'
	mov byte [gs:0x05],0xa4


	mov eax,LOADER_START_SECTOR
	mov bx, LOADER_BASE_ADDR
	mov cx, LOADER_SECTOR_COUNT
	call rd_disk_m_16

	jmp LOADER_BASE_ADDR


; 从硬盘读取n个扇区
; eax=起始的LBA扇区号
; ebx=数据写入的目标内存地址
; ecx=需要读入的扇区数
rd_disk_m_16:
	mov esi,eax
	mov di,cx

	mov dx,0x1f2 ;端口号 0x1f2，主通道读取时，用于指定读取的扇区数
	mov al,cl
	out dx,al

	mov eax,esi ; 恢复 eax

	mov dx,0x1f3 ; 指定起始扇区号的 LBA底8位
	out dx,al

	mov cl,8
	shr eax,cl
	mov dx,0x1f4 ; 指定起始扇区号的 LBA的中间8位
	out dx,al

	shr eax,cl
	mov dx, 0x1f5 ;指定起始扇区号的 LBA高8位
	out dx,al

	shr eax,cl
	and al,0x0f ; lba的 24 - 27位
	or al,0xe0  ;设置 al的高4位为 1110，表示lba模式
	mov dx,0x1f6
	out dx,al

	mov dx,0x1f7	;往 0x1f7端口写入 0x20后，硬盘开始读取
	mov al,0x20
	out dx,al		;此时，硬盘开始工作

;开始检测硬盘状态
.not_ready:		
	nop
	in al,dx	;依旧使用 0x1f7端口，可以获取到硬盘的工作状态
	and al,1000_1000b ;第4位为1 表示硬盘控制器已经准备好数据传输，第8位为1，表示硬盘忙碌
	cmp al,0000_1000b ; 预期结果为第4位为1（准备好），第8位为0（不忙碌）
	jnz .not_ready	;不满足上述条件时，继续等待

;开始读取数据
	;计算需要读取的次数，
	mov ax,di	;读取的扇区数
	mov dx,256
	mul dx ;每次可以读取2个字节，每个扇区为512字节，所以这里乘上 256
			   ;得到了需要读取的次数
	mov cx,ax  ;将读取次数放入到loop的循环次数寄存器 cx

	mov dx,0x1f0 ;从 0x1f0端口读取数据
.go_on_read:
	in ax,dx
	mov [bx],ax
	add bx,2
	loop .go_on_read
	ret


	times 510 - ($ - $$) db 0
	db 0x55,0xaa
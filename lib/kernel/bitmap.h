#ifndef __LIB_BITMAP_H
#define __LIB_BITMAP_H
#include "stdint.h"
#include "global.h"

#define BITMAP_MASK 1

struct bitmap{
    uint32_t btmp_bytes_len; //位图大小，单位是字节
    uint8_t *bits; // 
};

/**
 * 位图初始化
*/
void bitmap_init(struct bitmap* btmp);

/**
 * 用来确定位图的某一位是1，还是0。
 * 若是1，返回真（返回的值不一定是1）。
 * 否则，返回0。
 * 传入两个参数，指向位图的指针，与要判断的位的偏移
*/
bool bitmap_scan_test(struct bitmap* btmp, uint32_t bit_idx);

/**
 * 用来在位图中找到cnt个连续的0，以此来分配一块连续未被占用的内存，
 * 参数有指向位图的指针与要分配的内存块的个数cnt
 * 成功就返回起始位的偏移（如果把位图看做一个数组，那么也可以叫做下标），
 * 不成功就返回-1
*/
int bitmap_scan(struct bitmap* btmp, uint32_t cnt);

/**
 * 将位图某一位设定为1或0，传入参数是指向位图的指针与这一位的偏移，与想要的值
*/
void bitmap_set(struct bitmap* btmp, uint32_t bit_idx, int8_t value);

#endif
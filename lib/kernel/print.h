#ifndef __LIB_KERNEL_PRINT_H
#define __LIB_KERNEL_PRINT_H

#include "stdint.h"
void put_char(uint8_t c);

void put_str(uint8_t *c);

void put_int(uint32_t num);

void set_cursor_pos(uint32_t pos);
#endif
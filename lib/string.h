#ifndef __LIB_STRING_H
#define __LIB_STRING_H
#include "stdint.h"

/**
 * 将dst起始的size个字节置为value，
 * 这个函数最常用的用法就是来初始化一块内存区域，也就是置为ASCII码为0
 * dst_ : 目标字符串
 * value ： 目标值
 * size ： 字符串长度（不包括最后的 '\0'）
*/
void memset(void* dst_, uint8_t value, uint32_t size);

/**
 * 将src地址起始处size字节的数据移入dst，用于拷贝内存数据
 * src起始是有数据的，所以用const void*，const修饰void*，意味着地址内的数据是只读
 * size ： 字符串长度（不包括最后的 '\0'）
*/
void memcpy(void* dst_, const void* src_, uint32_t size);

/**
 * 比较两个地址起始的size字节的数据是否相等，如果相等，则返回0；
 * 如果不相等，比较第一个不相等的数据，>返回1，<返回-1
 * size ： 字符串长度（不包括最后的 '\0'）
*/
int memcmp(const void* a_, const void* b_, uint32_t size);

/**
 * 将字符串从src拷贝到dst,并返回目的字符串的起始地址
*/
char* strcpy(char* dst_, const char* src_);

/**
 * 返回字符串长度
*/
uint32_t strlen(const char* str);

/**
 * 比较两个字符串，若a_中的字符与b_中的字符全部相同，则返回0，
 * 如果不同，那么比较第一个不同的字符，如果a_>b_返回1，反之返回-1
*/
int8_t strcmp (const char *a, const char *b); 

/**
 * 从左到右查找字符串str中首次出现字符ch的地址(不是下标,是地址)
*/
char* strchr(const char* string, const uint8_t ch);

/**
 * 从右到左查找字符串str中首次出现字符ch的地址(不是下标,是地址)
*/
char* strrchr(const char* string, const uint8_t ch);

/**
 * 将字符串src_拼接到dst_后,将回拼接的串地址
*/
char* strcat(char* dst_, const char* src_);

/**
 *  在字符串str中查找指定字符ch出现的次数
*/
uint32_t strchrs(const char* filename, uint8_t ch);
#endif


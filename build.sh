nasm -o ./build/loader ./boot/loader.s
nasm -o ./build/mbr ./boot/mbr.s


nasm -o ./build/print.o -f elf ./lib/kernel/print.s
nasm -o ./build/kernel.o -f elf ./kernel/kernel.s
gcc -o ./build/interrupt.o -c  -fno-builtin -fno-stack-protector -m32 -I ./lib/kernel/ -I lib/ -I ./kernel ./kernel/interrupt.c
gcc -o ./build/init.o -c -fno-builtin -m32 -I ./lib/kernel/ -I lib/ -I device/ -I ./kernel ./kernel/init.c
gcc -o ./build/timer.o -c -fno-builtin -m32 -I ./lib/kernel/ -I lib/ -I ./kernel ./device/timer.c
gcc -o ./build/main.o -c  -fno-builtin -m32 -I ./lib/kernel/ -I ./kernel/ ./kernel/main.c 

ld -Ttext 0xc0001500 -e main -o ./build/kernel.bin -m elf_i386 ./build/main.o ./build/print.o ./build/kernel.o  ./build/interrupt.o  ./build/init.o ./build/timer.o

dd if=./build/mbr of=./bochs/hd60M.img bs=512 count=1 conv=notrunc
dd if=./build/loader of=./bochs/hd60M.img bs=512 seek=2 count=4 conv=notrunc 
dd if=./build/kernel.bin of=./bochs/hd60M.img bs=512 count=200 conv=notrunc seek=9



rm -f ./build/loader
rm -f ./build/mbr
rm -f ./build/kernel
# rm -f ./build/kernel.bin
rm -f ./build/*.o
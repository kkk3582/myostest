#include "gdtutil.h"
#include "../lib/stdint.h"
#include "../kernel/global.h"

void make_gdt_desc(struct gdt_desc *desc,uint32_t* desc_base_p, uint32_t limit, uint8_t attr_low, uint8_t attr_high) {
   uint32_t desc_base = (uint32_t)desc_base_p;
   desc->limit_low_word = limit & 0x0000ffff;
   desc->base_low_word = desc_base & 0x0000ffff;
   desc->base_mid_byte = ((desc_base & 0x00ff0000) >> 16);
   desc->attr_low_byte = (uint8_t)(attr_low);
   desc->limit_high_attr_high = (((limit & 0x000f0000) >> 16) + (uint8_t)(attr_high));
   desc->base_high_byte = desc_base >> 24;
}

void reload_gdt(uint32_t gtcCount)
{
   uint64_t gdt_operand =  ( (uint64_t)  (uint32_t)GDT_BASE_ADDR << 16 ) | ( gtcCount * 8 - 1 );
   asm volatile ("lgdt %0" : : "m" (gdt_operand));
}
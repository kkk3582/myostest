#ifndef __UTIL_GDTUTIL_H
#define __UTIL_GDTUTIL_H

#include "../kernel/global.h"



/**用于创建gdt描述符，
 * 传入参数1，段基址，
 * 传入参数2，段界限；
 * 参数3，属性低字节，
 * 参数4，属性高字节(要把低四位置0，高4位才是属性)
 */
void make_gdt_desc(struct gdt_desc *desc,uint32_t* desc_base_p, uint32_t limit, uint8_t attr_low, uint8_t attr_high) ;

void reload_gdt(uint32_t gtcCount);

#endif
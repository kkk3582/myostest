#ifndef __UTIL_MATHUTIL_H
#define __UTIL_MATHUTIL_H

#define DIV_ROUND_UP(X, STEP) ((X + STEP - 1) / (STEP))  //用于向上取整的宏，如9/10=1

#endif
#ifndef __USERPROG_PROCESS_H
#define __USERPROG_PROCESS_H
#include "../thread/thread.h"

#define USER_VADDR_START 0x8048000	 //linux下大部分可执行程序的入口地址（虚拟）都是这个附近，我们也仿照这个设定
#define KERNEL_VADDR_START 0xc0000000 //内核在虚拟地址中的起始地址

#define DEFAULT_PRIO  31

struct list ready_tasks;
struct list all_tasks;

void create_user_vaddr_bitmap(struct task_struct* user_prog);

//用于为进程创建页目录表，并初始化（系统映射+页目录表最后一项是自己的物理地址，以此来动态操作页目录表），成功后，返回页目录表虚拟地址，失败返回空地址
uint32_t* create_page_dir(void);

/**
 * 激活进程，为启动进程做准备
 * 1. 切换页表
 * 2. 更新 tss中的esp0
*/
void active_process(struct task_struct *pthread);

struct task_struct* process_execute(void* filename_, char *name);

#endif
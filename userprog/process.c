#include "process.h"
#include "../kernel/debug.h"
#include "../kernel/global.h"
#include "../util/mathUtil.h"
#include "../kernel/memory.h"
#include "../kernel/bitmap.h"
#include "../kernel/debug.h"
#include "../lib/string.h"
#include "tss.h"
#include "../lib/kernel/print.h"
#include "../kernel/interrupt.h"
#include "../util/gdtutil.h"
extern void intr_end(void);

void create_user_vaddr_bitmap(struct task_struct* user_prog)
{
    ASSERT(user_prog != NULL);
   

    uint32_t userMemByteCnt = KERNEL_VADDR_START - USER_VADDR_START;
    uint32_t userMemPageCnt = userMemByteCnt  / PAGE_SIZE;
    uint32_t bisCnt = userMemPageCnt / 8;  //位图使用 byte类型对内存进行管理，byte类型有8位，所以每个byte类型可以管理 8页

    uint32_t bitsPages = DIV_ROUND_UP(userMemByteCnt  / PAGE_SIZE / 8,PAGE_SIZE); //位图所需要的虚拟内存页数，使用了向上取整

    void * vaddrBitmapAddr = malloc_kernel_page(bitsPages); //从内核中申请相应的内存页，用于存放位图。

    struct virtual_addr * vaddr = & (user_prog->vaddr);
    vaddr->vaddr_start = USER_VADDR_START;
    vaddr->vaddr_bitmap.btmp_bytes_len = bisCnt;
    vaddr->vaddr_bitmap.bits = (uint8_t*) vaddrBitmapAddr;
    

    bitmap_init(&vaddr->vaddr_bitmap);
}

uint32_t* create_page_dir(void)
{
    uint32_t * pdePageStartAddr = malloc_kernel_page(1);//申请一页内存，左右页目录表
    memset(pdePageStartAddr,0,PAGE_SIZE);
    ASSERT( pdePageStartAddr != NULL);
    //从当前进程的 3GB位置，复制内核页目录项[768 , 1023）
    // 最后一项存放本页目录的起始地址。

    memcpy( (void*)((uint32_t)pdePageStartAddr + 768 * 4),
            (void*)(0xfffff000 + 768 * 4),
            255 * 4);

    pdePageStartAddr[1023] = addr_v_to_p((uint32_t)pdePageStartAddr)  | PG_US_U | PG_RW_W | PG_P_1;
    
    return pdePageStartAddr;
}


void start_process(void* filename_)
{

    void *function = filename_;

    struct task_struct *currentTask =  current_thread();
    struct intr_stack *proc_stack = (struct intr_stack *)((uint32_t)currentTask->self_kstack + sizeof(struct thread_stack ));
    currentTask->self_kstack =(uint32_t*) proc_stack;
    proc_stack->edi = proc_stack->esi = proc_stack->ebp = proc_stack->esp_dummy = 0;
    proc_stack->ebx = proc_stack->edx = proc_stack->ecx = proc_stack->eax = 0;
    proc_stack->gs = 0;		 //用户态根本用不上这个，所以置为0（gs我们一般用于访问显存段，这个让内核态来访问）
    proc_stack->ds = proc_stack->es = proc_stack->fs = SELECTOR_U_DATA; 
    proc_stack->eip = function;
    proc_stack->cs = SELECTOR_U_CODE;
    proc_stack->ss = SELECTOR_U_DATA;
    proc_stack->eflags = (EFLAGS_IOPL_0 | EFLAGS_MBS | EFLAGS_IF_1); 

    void* stackMemPageStartAddr = malloc_one_page_by_special_vaddr(PT_USER,KERNEL_VADDR_START - PAGE_SIZE);
    proc_stack->esp = (void*)((uint32_t)stackMemPageStartAddr + PAGE_SIZE);

    asm volatile ("movl %0, %%esp; jmp intr_end" : : "g" (proc_stack) : "memory");
}


 /********************************************************
 * 用于加载进程自己的页目录表，同时更新进程自己的0特权级esp0到TSS中
 * 执行此函数时,当前任务可能是线程。
 * 之所以对线程也要重新安装页表, 原因是上一次被调度的可能是进程,
 * 否则不恢复页表的话,线程就会使用进程的页表了。
 ********************************************************/
void active_process(struct task_struct *pthread)
{

    uint32_t pdAddr = PAGE_DIR_TABLE_POS; // 默认为内核的页目录物理地址,也就是内核线程所用的页目录表
    if( pthread->pgaddr != NULL  ){
        pdAddr = addr_v_to_p(pthread->pgaddr);
        update_tss_esp0(pthread);
    }

     /* 激活该进程或线程的页表 */
    asm volatile ("movl %0, %%cr3": : "r"(pdAddr):"memory");
}


struct task_struct* process_execute(void* filename_, char *name)
{

    struct task_struct* pthread = malloc_kernel_page(1);

    init_thread(pthread,name,DEFAULT_PRIO);
    create_user_vaddr_bitmap(pthread); //初始化用户进程虚拟内存位图
    thread_create(pthread,start_process,filename_);

    pthread->pgaddr = create_page_dir(); //创建用户进程页表



    enum intr_status old_status = intr_disable();
    ASSERT( !elem_find( &ready_tasks,&(pthread->generalListElement)));
    list_append(&ready_tasks,&pthread->generalListElement);

    ASSERT( !elem_find( &all_tasks,&pthread->allListElement));
    list_append(&all_tasks,&pthread->allListElement);
    set_intr_status(old_status);
   return pthread;
}
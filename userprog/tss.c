#include "tss.h"
#include "../lib/string.h"
#include "../kernel/global.h"
#include "../lib/kernel/print.h"
#include "../util/gdtutil.h"
#include "../lib/kernel/print.h"


struct tss{
    uint32_t prevTss;
    uint32_t* esp0;
    uint32_t ss0;
    uint32_t* esp1;
    uint32_t ss1;
    uint32_t* esp2;
    uint32_t ss2;
    uint32_t cr3;
    uint32_t (*eip) (void);
    uint32_t eflags;
    uint32_t eax;
    uint32_t ecx;
    uint32_t edx;
    uint32_t ebx;
    uint32_t esp;
    uint32_t ebp;
    uint32_t esi;
    uint32_t edi;
    uint32_t es;
    uint32_t cs;
    uint32_t ss;
    uint32_t ds;
    uint32_t fs;
    uint32_t gs;
    uint32_t ldtSelector;
    uint32_t ioBitmapOffset;
};

static struct tss tss_struct;
static struct gdt_desc gdtDesc;


/**
 * 初始化 tss结构体
*/
static void init_tss(struct tss* tssp)
{
    uint32_t tssSize = sizeof(struct tss);
    memset(tssp,0,tssSize);
    tssp->ss0 = SELECTOR_K_STACK;
    tssp->ioBitmapOffset = tssSize; 
}

static void load_tss(uint32_t selectorTss){
    asm volatile ("ltr %w0" : : "r" (selectorTss));
}


/**
 * 初始化tss信息
*/
void tss_init(void)
{
    put_str("tss init start\n");


    init_tss(&tss_struct);
    struct gdt_desc *tssGdtDesc = GET_GDT(SELECTOR_TSS>>3);
    make_gdt_desc(tssGdtDesc,&tss_struct,sizeof(struct tss) - 1,TSS_ATTR_LOW, TSS_ATTR_HIGH);

    make_gdt_desc(GET_GDT(SELECTOR_U_CODE>>3),(uint32_t *)0,0xfffff,GDT_CODE_ATTR_LOW_DPL3, GDT_ATTR_HIGH);
    make_gdt_desc(GET_GDT(SELECTOR_U_DATA>>3),(uint32_t *)0,0xfffff, GDT_DATA_ATTR_LOW_DPL3, GDT_ATTR_HIGH);

    reload_gdt(7); //目前一共7个gdt描述符，0号，3个内核用描述符，3个用户进程用，1一个tss
    load_tss(SELECTOR_TSS);
    put_str("tss init done\n");
}


void update_tss_esp0(struct task_struct *pthread)
{
    tss_struct.esp0 = (uint32_t*)((uint32_t)pthread + PAGE_SIZE);
}
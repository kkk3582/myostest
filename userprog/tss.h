#ifndef __USERPROG_TSS_H
#define __USERPROG_TSS_H

#include "../lib/stdint.h"
#include "../thread/thread.h"


void tss_init(void);

void update_tss_esp0(struct task_struct *pthread);

#endif

#include "syscall_init.h"
#include "../lib/kernel/print.h"
#include "syscall.h"
#include "../thread/thread.h"

#define SYSCALL_CNT 10

typedef void* syscall;


syscall syscall_handler_table[SYSCALL_CNT];

static void init_syscall_handler_table(void);


static uint16_t sys_get_pid(void);

void syscall_init(void)
{
    put_str("syscall init start.\n");
    init_syscall_handler_table();

    put_str("syscall init done.\n");
}


static void init_syscall_handler_table(void)
{
    syscall_handler_table[SYSCALL_NR_GET_PID] = sys_get_pid;
}


static pid_t sys_get_pid(void)
{
    return current_thread()->pid;
}
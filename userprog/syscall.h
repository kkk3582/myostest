#ifndef __USERPROG_SYSCALL_H
#define __USERPROG_SYSCALL_H
#include "../thread/thread.h"



enum SYSCALL_NR{
    SYSCALL_NR_GET_PID
};



pid_t getPid(void);


#endif
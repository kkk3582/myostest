#include "init.h"
#include "../lib/kernel/print.h"
#include "interrupt.h"
#include "timer.h"
#include "memory.h"
#include "../thread/thread.h"
#include "../device/console.h"
#include "../device/keyboard.h"
#include "../userprog/tss.h"
#include "../userprog/syscall_init.h"

void init_all()
{
    put_str("init_all\n");
    idt_init();
    timer_init();
    mem_init();
    console_init();
    keyboard_init();
    tss_init();
    thread_init();
    syscall_init();
   
}
#ifndef __KERNEL_INTERRUPT_H
#define __KERNEL_INTERRUPT_H

#include "stdint.h"
typedef void * interrupt_handler;
enum intr_status{
    INTR_OFF,
    INTR_ON
};
void idt_init(void);

//返回开启中断前的状态
enum intr_status intr_enable(void);

//返回开启中断前的状态
enum intr_status intr_disable(void);

enum intr_status get_intr_status(void);

//返回中断状态修改前的状态
enum intr_status set_intr_status(enum intr_status intrStatus);

//通用的终端处理函数
void common_handler(uint32_t intrruptNo);

void register_handler(uint32_t intrruptNo,interrupt_handler handler);

#endif
#ifndef __KERNEL_MEMORY_H
#define __KERNEL_MEMORY_H
#include "../lib/stdint.h"
#include "../lib/kernel/bitmap.h"

#define	 PG_P_1	  1	// 页表项或页目录项存在属性位
#define	 PG_P_0	  0	// 页表项或页目录项存在属性位
#define	 PG_RW_R  0	// R/W 属性位值, 读/执行
#define	 PG_RW_W  2	// R/W 属性位值, 读/写/执行
#define	 PG_US_S  0	// U/S 属性位值, 系统级
#define	 PG_US_U  4	// U/S 属性位值, 用户级


struct virtual_addr{
    struct bitmap vaddr_bitmap;
    uint32_t vaddr_start;
};

/* 内存池标记,用于判断用哪个内存池 */
enum pool_type {
   PT_KERNEL = 1,    // 内核内存池
   PT_USER = 2	     // 用户内存池
};
void mem_init(void);


/**
 * 申请连续的内存页，并返回起始虚拟地址。
 * pt:申请的内存池类型
 * pcnt: 申请的连续内存页数量
*/
void* malloc_page(enum pool_type pt, uint32_t pcnt);


void* malloc_kernel_page( uint32_t pcnt);


/**
 * 将给定的虚拟地址转换成对应的物理地址
*/
uint32_t addr_v_to_p(uint32_t vaddr);


/**
 * 为传入的虚拟内存分配一页可用的物理内存。
 * 即：根据给定的虚拟内存地址，申请一页可用内存
*/
void* malloc_one_page_by_special_vaddr(enum pool_type pt, uint32_t vaddr);

#endif
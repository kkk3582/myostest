#include "../lib/kernel/print.h"
#include "../kernel/init.h"
#include "../kernel/interrupt.h"
#include "../thread/thread.h"
#include "../device/console.h"
#include "../device/keyboard.h"
#include "../userprog/process.h"
#include "../kernel/memory.h"
#include "../userprog/syscall.h"
void thread_a(void* args);
void thread_b(void* args);
void thread_c(void* args);
void process_a(void);
void process_b(void);

int a = 0 , b = 0;
int pida=0,pidb=0;
int main(){

  
    set_cursor_pos(0);
    put_str(" --Hello World!!! by kkk!!! \n");
    put_str("init start\n");
    init_all();

    process_execute(process_a,"process_a");
 
    process_execute(process_b,"process_b");

    thread_start("thread_a",30,thread_a,"A: ");
    thread_start("thread_b",30,thread_b,"B: ");
    thread_start("thread_c",30,thread_c,"C: ");

    intr_enable(); //临时开启中断
    while(1);
    return 0;
}


void thread_a(void* args)
{
    char* msg = (char*)args;
    while(1){
         int i = 9999999;
         while(i--);
         //console_put_char(kbd_get());
         console_put_str(args);
         console_put_str("0x");
         console_put_int(a);
        console_put_str(" --- pid-a:");
        console_put_int(pida);
         console_put_str("\n");

    }
    
}


void thread_b(void* args)
{
    char* msg = (char*)args;
    while(1){
         int i = 9999999;
         while(i--);
         //console_put_char(kbd_get());
        console_put_str(args);
         console_put_str("0x");
         console_put_int(b);
        console_put_str(" --- pid-b:");
        console_put_int(pidb);
         console_put_str("\n");

    }
    
}

void thread_c(void* args)
{
    char* msg = (char*)args;
    while(1){
         console_put_char(kbd_get());

    }
    
}

void process_a(void)
{
    pida = getPid();
    while(1){
        int i = 9999999;
         while(i--);
         a++;
    }
}

void process_b(void)
{
    pidb = getPid();
    while(1){
        int i = 9999999;
        while(i--);
        b++;
    }
}
[bits 32]
%define ERROR_CODE nop
%define ZERO    push 0          

extern put_str
extern idt_handler_table

section .data
intr_str db 'interrupt occur' ,0xa,0

global intr_entry_table
intr_entry_table:

%macro VECTOR 2
section .text
intr_%1_entry:
    %2
    push ds
    push es
    push fs
    push gs
    pushad
    mov al,0x20
    out 0xa0,al
    out 0x20,al
    
   
    push %1
    call [idt_handler_table + %1 * 4]

   
    jmp intr_end
  
section .data
    dd intr_%1_entry
%endmacro

section .text
global intr_end
intr_end:
    add esp,4 ;弹出 idt_table函数的第二个参数，中断号
    popad
    pop gs
 
    pop fs
    pop es
    pop ds

    add esp,4 ;弹出 idt_table函数的第一个参数，错误码
   
    iretd


VECTOR 0x00,ZERO                            ;调用之前写好的宏来批量生成中断处理函数，传入参数是中断号码与上面中断宏的%2步骤，这个步骤是什么都不做，还是压入0看p303
VECTOR 0x01,ZERO
VECTOR 0x02,ZERO
VECTOR 0x03,ZERO 
VECTOR 0x04,ZERO
VECTOR 0x05,ZERO
VECTOR 0x06,ZERO
VECTOR 0x07,ZERO 
VECTOR 0x08,ERROR_CODE
VECTOR 0x09,ZERO
VECTOR 0x0a,ERROR_CODE
VECTOR 0x0b,ERROR_CODE 
VECTOR 0x0c,ZERO
VECTOR 0x0d,ERROR_CODE
VECTOR 0x0e,ERROR_CODE
VECTOR 0x0f,ZERO 
VECTOR 0x10,ZERO
VECTOR 0x11,ERROR_CODE
VECTOR 0x12,ZERO
VECTOR 0x13,ZERO 
VECTOR 0x14,ZERO
VECTOR 0x15,ZERO
VECTOR 0x16,ZERO
VECTOR 0x17,ZERO 
VECTOR 0x18,ERROR_CODE
VECTOR 0x19,ZERO
VECTOR 0x1a,ERROR_CODE
VECTOR 0x1b,ERROR_CODE 
VECTOR 0x1c,ZERO
VECTOR 0x1d,ERROR_CODE
VECTOR 0x1e,ERROR_CODE
VECTOR 0x1f,ZERO 
VECTOR 0x20,ZERO
VECTOR 0x21,ZERO


extern syscall_handler_table ; 系统调用表

;0x80 中断处理函数,根据eax中的系统调用号，执行对应的系统调用函数
[bits 32]
section .text
global syscall_handler
syscall_handler:
    ZERO
    push ds
    push es
    push fs
    push gs
    pushad ; PUSHAD指令压入32位寄存器，其入栈顺序是:EAX,ECX,EDX,EBX,ESP,EBP,ESI,EDI
    push 0x80  ;push 中断号，0x80

    push edx    ;第三个参数
    push ecx    ;第二个参数
    push ebx    ;第一个参数
    call [syscall_handler_table + eax * 4]
    add esp, 12 ; 跳过三个参数
    mov [esp + 8*4] , eax ; [esp + 8*4] 处存放的是，发生中断时的内核栈 esp的值，该行的意思是，将eax（即，返回值）放入到 发生中断时的内核栈顶，即，作为实际调用处的返回值使用。
   
    jmp intr_end

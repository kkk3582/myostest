BUILD_DIR=./build
ENTRY_POINT=0xc0001500
HD60M_PATH=./bochs/hd60M.img

AS=nasm
CC=gcc
LD=ld
LIB = -I lib/ -I lib/kernel -I kernel/ -I device/ -I thread/
ASFLAGS= -f elf -g
CFLAGS= -Wall $(LIB) -c -fno-builtin -W -Wstrict-prototypes -Wmissing-prototypes -m32 -fno-stack-protector -g
LDFLAGS = -Ttext $(ENTRY_POINT) -e main -Map $(BUILD_DIR)/kernel.map -m elf_i386 


OBJS= $(BUILD_DIR)/main.o $(BUILD_DIR)/init.o \
	$(BUILD_DIR)/interrupt.o $(BUILD_DIR)/timer.o $(BUILD_DIR)/kernel.o \
	$(BUILD_DIR)/print.o $(BUILD_DIR)/debug.o $(BUILD_DIR)/string.o $(BUILD_DIR)/bitmap.o \
	$(BUILD_DIR)/memory.o $(BUILD_DIR)/process.o $(BUILD_DIR)/list.o $(BUILD_DIR)/switch.o \
	$(BUILD_DIR)/sync.o  $(BUILD_DIR)/console.o $(BUILD_DIR)/keyboard.o $(BUILD_DIR)/ioseq.o \
	$(BUILD_DIR)/tss.o $(BUILD_DIR)/gdtutil.o $(BUILD_DIR)/thread.o $(BUILD_DIR)/syscall_init.o \
	$(BUILD_DIR)/syscall.o

.PHONY: mk_dir boot hd all build clean

mk_dir:
	if [ ! -d $(BUILD_DIR) ];then mkdir $(BUILD_DIR);fi


################################## BOOT FILE ############################
boot:$(BUILD_DIR)/mbr.o $(BUILD_DIR)/loader.o
$(BUILD_DIR)/mbr.o:boot/mbr.s
	$(AS) -I boot/include/ -o ./build/mbr ./boot/mbr.s
$(BUILD_DIR)/loader.o:boot/loader.s	
	$(AS) -I boot/include/  -o ./build/loader ./boot/loader.s

################################# C kernel ##############################
$(BUILD_DIR)/main.o:kernel/main.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<
$(BUILD_DIR)/init.o:kernel/init.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<
$(BUILD_DIR)/interrupt.o:kernel/interrupt.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<		
$(BUILD_DIR)/debug.o:kernel/debug.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<			
$(BUILD_DIR)/memory.o:kernel/memory.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<	
################################# C Lib ##############################
$(BUILD_DIR)/string.o:lib/string.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<	
$(BUILD_DIR)/bitmap.o:lib/kernel/bitmap.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<		
$(BUILD_DIR)/list.o:lib/kernel/list.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<		

################################# C Device ##############################
$(BUILD_DIR)/timer.o:device/timer.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<
$(BUILD_DIR)/console.o:device/console.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<
$(BUILD_DIR)/keyboard.o:device/keyboard.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<	
$(BUILD_DIR)/ioseq.o:device/ioseq.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<		
################################# C Thread ##############################
$(BUILD_DIR)/thread.o:thread/thread.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<	
$(BUILD_DIR)/sync.o:thread/sync.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<	

################################# C Userprog ##############################
$(BUILD_DIR)/tss.o:userprog/tss.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<	
$(BUILD_DIR)/process.o:userprog/process.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<		
$(BUILD_DIR)/syscall_init.o:userprog/syscall_init.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<	
$(BUILD_DIR)/syscall.o:userprog/syscall.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<	
################################# C Util ##############################
$(BUILD_DIR)/gdtutil.o:util/gdtutil.c
	$(CC) $(CFLAGS) $(LIB) -o $@ $<		

################################# AS Thread #############################
$(BUILD_DIR)/switch.o:thread/switch.s
	$(AS) $(ASFLAGS) -o $@ $<

################################# AS kernel #############################
$(BUILD_DIR)/print.o:lib/kernel/print.s
	$(AS) $(ASFLAGS) -o $@ $<
$(BUILD_DIR)/kernel.o:kernel/kernel.s
	$(AS) $(ASFLAGS) -o $@ $<
################################# LD kernel #############################
$(BUILD_DIR)/kernel.bin:$(OBJS)
	$(LD) $(LDFLAGS) -o $@ $^

hd:
	dd if=./build/mbr of=$(HD60M_PATH) bs=512 count=1 conv=notrunc
	dd if=./build/loader of=$(HD60M_PATH)  bs=512 seek=2 count=4 conv=notrunc 
	dd if=./build/kernel.bin of=$(HD60M_PATH)  bs=512 count=200 conv=notrunc seek=9

gdb_symbol:
	objcopy --only-keep-debug $(BUILD_DIR)/kernel.bin $(BUILD_DIR)/kernel.sym

build:$(BUILD_DIR)/kernel.bin

all:mk_dir boot build hd gdb_symbol

clean:
	rm -rfv ./build/*.o
	rm -rfv ./build/loader
	rm -rfv ./build/mbr
	rm -rfv ./build/kernel.bin
	rm -rfv ./build/kernel.map
	rm -rfv ./build/kernel.sym
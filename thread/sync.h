#ifndef __THREAD_SYNC_H
#define __THREAD_SYNC_H

#include "stdint.h"
#include "list.h"
#include "thread.h"

/**
 * 信号量结构体
*/
struct semaphore {
    uint8_t value; // 信号量数值，当数值小于等于0是，需要进入下面的等待队列。
    struct list waiters;
};

/**
 * 锁结构体
*/
struct lock{
    struct task_struct* holder; //当前持有者
    struct semaphore semaphore; //通过 lock结构体，间接的管理 信号量。
    uint32_t holderCount; //当前持有者，所持有的锁次数。
};

void sema_init(struct semaphore* psema, uint8_t value); 
void sema_down(struct semaphore* psema);
void sema_up(struct semaphore* psema);


void lock_init(struct lock* plock);
void lock_acquire(struct lock* plock);
void lock_release(struct lock* plock);

#endif
#ifndef __THREAD_THREAD_H
#define __THREAD_THREAD_H
#include "../lib/stdint.h"
#include "../lib/kernel/list.h"
#include "../kernel/memory.h"

#define STACK_MAGIC 0x19880114
#define PCB_CHECK_MAGIC(taskStruct) (taskStruct->stack_magic == STACK_MAGIC)

typedef uint16_t pid_t;

extern struct list ready_tasks; //定义在 userprog/process.c中
extern struct list all_tasks;


typedef void thread_func(void*args);

enum task_status{
    TASK_RUNNING,
    TASK_READY,
    TASK_BLOCKED,
    TASK_WATTING,
    TASK_HANGING,
    TASK_DIED
};

                               /***********   中断栈intr_stack   ***********
                                 * 此结构用于中断发生时保护程序(线程或进程)的上下文环境:
                                 * 进程或线程被外部中断或软中断打断时,会按照此结构压入上下文
                                 * 寄存器,  intr_exit中的出栈操作是此结构的逆操作
                                 * 此栈在线程自己的内核栈中位置固定,所在页的最顶端
                                ********************************************/
struct intr_stack {
    uint32_t vec_no;	        // kernel.S 宏VECTOR中push %1压入的中断号
    uint32_t edi;
    uint32_t esi;
    uint32_t ebp;
    uint32_t esp_dummy;	        // 虽然pushad把esp也压入,但esp是不断变化的,所以会被popad忽略
    uint32_t ebx;
    uint32_t edx;
    uint32_t ecx;
    uint32_t eax;
    uint32_t gs;
    uint32_t fs;
    uint32_t es;
    uint32_t ds;

                                /* 以下由cpu从低特权级进入高特权级时压入 */
    uint32_t err_code;		    // err_code会被压入在eip之后
    void (*eip) (void);
    uint32_t cs;
    uint32_t eflags;
    void* esp;
    uint32_t ss;
};
                                /***********  线程栈thread_stack  ***********
                                 * 线程自己的栈,用于存储线程中待执行的函数
                                 * 此结构在线程自己的内核栈中位置不固定,
                                 * 用在switch_to时保存线程环境。
                                 * 实际位置取决于实际运行情况。
                                 ******************************************/
struct thread_stack {
   uint32_t ebp;
   uint32_t ebx;
   uint32_t edi;
   uint32_t esi;

                                    //这个位置会放一个名叫eip，返回void的函数指针(*epi的*决定了这是个指针)，
                                    //该函数传入的参数是一个thread_func类型的函数指针与函数的参数地址
   void (*eip) (thread_func* func, void* func_arg);

                                     //以下三条是模仿call进入thread_start执行的栈内布局构建的，call进入就会压入参数与返回地址，因为我们是ret进入kernel_thread执行的
                                    //要想让kernel_thread正常执行，就必须人为给它造返回地址，参数
   void (*unused_retaddr);
   thread_func* function;           // Kernel_thread运行所需要的函数地址
   void* func_arg;                  // Kernel_thread运行所需要的参数地址
};

                                    /* 进程或线程的pcb,程序控制块, 此结构体用于存储线程的管理信息*/
struct task_struct {
   uint32_t* self_kstack;	        // 用于存储线程的栈顶位置，栈顶放着线程要用到的运行信息
   enum task_status status;
   uint8_t priority;		        // 线程优先级
   uint8_t tick;
   uint8_t escapTick;
   pid_t pid;
   char name[16];                   //用于存储自己的线程的名字
   struct list_elem generalListElement; 
   struct list_elem allListElement;
   struct virtual_addr vaddr;   //用户进程的4G虚拟地址空间管理结构体
   uint32_t* pgaddr;     // page table addr
   uint32_t stack_magic;	       //如果线程的栈无限生长，总会覆盖地pcb的信息，那么需要定义个边界数来检测是否栈已经到了PCB的边界
};

/**用于根据传入线程的pcb地址、要运行的函数地址、函数的参数地址来初始化线程栈中的运行信息，
 * 核心就是填入要运行的函数地址与参数 
 */
void thread_create(struct task_struct* pthread, thread_func function, void* func_arg);

/** 初始化线程基本信息 , 
 * pcb中存储的是线程的管理信息，
 * 此函数用于根据传入的pcb的地址，
 * 线程的名字等来初始化线程的管理信息
*/
void init_thread(struct task_struct* pthread, char* name, int prio);


/* 创建一优先级为prio的线程,线程名为name,线程所执行的函数是function(func_arg) */
struct task_struct* thread_start(char* name, int prio, thread_func function, void* func_arg);

/**
 * 
*/
void thread_init(void);

void schedule(void);

struct task_struct* current_thread(void);


/**
 * 将当前正在运行的线程pcb中的状态字段设定为传入的status,一般用于线程主动设定阻塞.
 * 并调用schedule() 进行切换
*/
void thread_block(enum task_status status);

/**
 * 将传入的线程状态修改为 TASK_READY
 * 并调添加到 ready队列头部，等待下一次调度
*/
void thread_unblock(struct task_struct *pthread);

#endif
#ifndef __DEVICE_IOSEQ_H
#define __DEVICE_IOSEQ_H

#include "../kernel/global.h"
#include "../thread/sync.h"
#include "../thread/thread.h"

#define IO_SEQ_BUF_SIZE 3

struct io_seq{
    uint8_t codeBuf[IO_SEQ_BUF_SIZE]; //这里不需要考虑拓展码合并后的情况，只用于接收单次键盘中断发送的编码，所有使用了 8位
    struct lock io_lock;
    uint8_t pos;    //数据指针当前位置
    uint8_t limit;  //数据指针最大位置，即最新的数据存放点
    struct task_struct *consumer;
    struct task_struct *provider;
};


void io_seq_init(struct io_seq*);

/**
 * 从末尾获取一个值
*/
uint8_t io_seq_get(struct io_seq*);

/**
 * 将val 添加到头部
*/
void io_seq_add(struct io_seq*,uint8_t val);

/**
 * 判断一个io seq是否为空
*/
bool io_seq_empty(struct io_seq*);


/**
 * 判断一个io seq是否满了
*/
bool io_seq_full(struct io_seq*);


#endif
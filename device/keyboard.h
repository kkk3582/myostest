#ifndef __DEVICE_KEYBOARD_H
#define __DEVICE_KEYBOARD_H
#include "../lib/stdint.h"
void keyboard_init();

/**
 * 从键盘缓存区中，获取一个数据。
*/
uint8_t kbd_get(void);


#endif